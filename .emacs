;;; .emacs --- Emacs configuration file
;;; Commentary:
;; Basic Emacs config file that loads the package interface and then
;; installs and configures a selection of often used packages.
;;
;; This config file assumes you are using Emacs 29.x
;;; Code:

(require 'package)
;; Add the most commonly used Package archives
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)


(setq use-package-verbose t)
(eval-when-compile 'use-package)

;;;;********************************************************
;;;; Some general customisations:
;;;;********************************************************
(use-package emacs
  :custom
  (inhibit-startup-message t)

  ;; Show column & line numbers in mode bar
  (column-number-mode t)

  ;; Tell emacs where to read abbrev definitions from
  (abbrev-file-name "~/.emacs.d/abbrev_defs")

  ;; Code display options (highlight parens)
  (show-paren-mode 1)

  ;; Three options for paren-style: 'expression, 'parenthesis, and
  ;; 'mixed first one highlights complete region between parens,
  ;; second only highlights matching paren, third does 'expression
  ;; when matching paren is not visible.
  (show-paren-style 'mixed)

  :config
  ;;; Set the text for window's title bar and icons, %f=filename, %b=buffername
  (setq frame-title-format '(buffer-file-name "%f" "%b"))
  (setq icon-title-format frame-title-format)

  ;; Backups
  ;; Stop leaving backup~ files scattered everywhere, put
  ;; them in one dir and use version control, etc.
  (setq make-backup-files t              ; do make backups
        backup-by-copying t              ; and copy them here
        version-control t                ; version numbers for the files
        kept-new-versions 2
        kept-old-versions 3
        delete-old-versions t            ; delete excess backups files
        )
  (setq backup-directory-alist '(("." . "~/.emacs.d/backups")) )
  )


;;; Those on an Apple Mac will likely want to adjust some key
;;; mappings:
(when (eq system-type 'darwin)
  ;; Don't let Emacs claim the right option key free so it stays under
  ;; macOS' control and can be used to enter characters like ~ and |.
  (setq mac-right-option-modifier 'none)

  ;; For other key remappings have a look at
  ;; https://nickdrozd.github.io/2019/12/28/emacs-mac-mods.html.
  ;; Possible values instead of 'none are 'control, 'alt, meta,
  ;; 'super, 'hyper. You might see the keys referred to as ns- in
  ;; place of mac- in some places. The ns-, I believe, is referring to
  ;; NeXTSTEP, one of the sources for OSX

  ;; List of keys:
  ;; mac-function-modifier
  ;; mac-control-modifier
  ;; mac-command-modifier
  ;; mac-option-modifier
  ;; mac-right-command-modifier
  ;; mac-right-control-modifier
  ;; mac-right-option-modifier
  )

;;; For those on Apple Mac: when launching from the menu bar, the PATH
;;; is different compared to when Emacs is launched from a terminal.
;;; They should be identical... See
;;; https://github.com/purcell/exec-path-from-shell
(when (memq window-system '(mac ns))
  (use-package exec-path-from-shell
    :ensure t
    :config
    (exec-path-from-shell-initialize)
    )
  )


;;;;;;;;;;;;;;;;;;;;
;;;; White space stuff
;; Don't ever insert tabs when indenting regions (setq-default is used
;; here to only overwrite this in buffers that do not have their own
;; local values for this variable)
(setq-default indent-tabs-mode nil)
(use-package whitespace
  :ensure t
  :defer 5
  :custom
  ;; Types of whitespace to show
  (whitespace-style '(face trailing tabs indentation empty tab-mark))
  ;; Don't load whitespace-mode in the following buffer types:
  (whitespace-global-modes '(not magit-log-mode))
  :config
  (global-whitespace-mode)
  )


;;;;;;;;;;;;;;;;;;;;
;; Configure TRAMP (for accessing remote files)
(use-package tramp
  :defer t
  :custom
  (tramp-use-controlmaster-options nil)
  :config
  ;; Add the $PATH variable on the remote host to tramp's list of
  ;; paths so that we can get to our own (precomiled) binaries.
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path)
  ;; Use a control master connection for SSH so we only need to
  ;; connect to the remote server once and reuse that connection.
  )


;;;;;;;;;;;;;;;;;;;;
;; Configure Vertico and friends for vertical completion in the minibuffer
;; https://github.com/minad/vertico
(use-package vertico
  :ensure t
  :custom
  ;; Enable cycling for `vertico-next' and `vertico-previous'.
  (setq vertico-cycle t)

  ;; Different scroll margin
  ;; (vertico-scroll-margin 0)

  ;; Show more candidates
  ;; (vertico-count 20)

  ;; Grow and shrink the Vertico minibuffer
  ;; (vertico-resize t)
  :init
  (vertico-mode)
  )

;; Use the `orderless' completion style.
(use-package orderless
  :ensure t
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles
                                               partial-completion))))
  )

;; Enable richer annotations using the Marginalia package
;; https://github.com/minad/marginalia
(use-package marginalia
  :ensure t
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :bind (         :map minibuffer-local-map
                  ("M-A" . marginalia-cycle))

  ;; The :init configuration is always executed (Not lazy!)
  :init
  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode)
  )

;; Enable Embark to add actions to minibuffer
;; https://github.com/oantolin/embark
(use-package embark
  :ensure t
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'
  :init
  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))
  )

;; Persist history over Emacs restarts. Vertico sorts by history
;; position.
(use-package savehist
  :init
  (savehist-mode))


;;;;;;;;;;;;;;;;;;;;
;; Configure Magit
;; https://magit.vc
(use-package magit
  :ensure t
  :bind
  ("C-x g" . magit-status)
  ("C-c g" . magit-file-dispatch)
  :custom
  ;; Set the following variable to nil in order to allow magit to show
  ;; whitespace differences
  (smerge-refine-ignore-whitespace nil)
  (magit-diff-refine-hunk `all)
  (magit-diff-refine-ignore-whitespace nil)
  )

(use-package forge
  :after magit
  :ensure t
  :config
  ;; Add PolyOmica Gitlab URL to list of forges
  (add-to-list 'forge-alist '("git.polyomica.com" "git.polyomica.com/api/v4" "git.polyomica.com" forge-gitlab-repository))
  )


;;;;;;;;;;;;;;;;;;;;
;; Enable diff-hl-mode (https://github.com/dgutov/diff-hl) to
;; highlight lines with uncommitted changes
(use-package diff-hl
  :ensure t
  :after magit
  :init (global-diff-hl-mode)
  :hook
  ((magit-pre-refresh . diff-hl-magit-pre-refresh)
   (magit-post-refresh . diff-hl-magit-post-refresh))
  (vc-checkin . diff-hl-update)
  )


;;; Show which keys are available for a given prefix. Note: this
;;; package will be part of Emacs in v30.
(use-package which-key
  :ensure t
  :custom
  ;; Set the time delay (in seconds) for the which-key popup to
  ;; appear. A value of zero might cause issues so a non-zero value is
  ;; recommended.
  (which-key-idle-delay 1.0)
  :hook
  (after-init . which-key-mode)
  )


;;;;;;;;;;;;;;;;;;;;
;; ESS (Emacs Speaks Statistics) for R
(use-package ess
  :ensure t
  :defer t
  :init
  (require 'ess-site)
  :commands R
  :mode ("\\.R\\'" . R-mode)
  :custom
  (inferior-R-args "--no-save ")
  ;; When hitting tab, first try to indent, else try to complete.
  (tab-always-indent 'complete)
  ;; Set indentation style
  (ess-style 'RStudio-)
  ;; Don't indent single # comments to col 40 and don't require double
  ;; ## for comments at the regular indent level.
  (ess-indent-with-fancy-comments nil)
  )


;;;;;;;;;;;;;;;;;;;;
;; Enable poly-R mode for R markdown files
;; https://github.com/polymode/poly-R
(use-package poly-R
  :ensure t
  :defer t
  )

;;;;;;;;;;;;;;;;;;;;
;; Enable the Eglot LSP client for various programming modes.
;; https://joaotavora.github.io/eglot/. As of Emacs 29.1, Eglot is
;; included in Emacs.
(use-package eglot
  :ensure t
  :hook
  (
   ;; Bash:
   ;; Install shellcheck (apt install shellcheck) and the Bash language
   ;; server (snap install bash-language-server --classic)
   (sh-mode . eglot-ensure)
   ;; Python:
   ;; Install with: pip3 install python-lsp-server
   (python-mode . eglot-ensure)
   ;; R
   ;; Install the "languageserver" R package
   (R-mode . eglot-ensure)
   ;; YAML
   ;; Install the language server: npm install -g yaml-language-server
   (yaml-mode . eglot-ensure)
   )
  :custom
  (eglot-autoshutdown t)
  )


;;;;;;;;;;;;;;;;;;;;
;; Use LSP to show a list of "symbols": functions and variables when
;; in programming modes
;; https://github.com/liushihao456/symbols-outline.el
(use-package symbols-outline
  :ensure t
  :bind
  (("C-c i" . symbols-outline-show))
  :custom
  ;; Show variables in the list as well
  (symbols-outline-ignore-variable-symbols nil)
  :config
  ;; Use LSP to find the symbols (instead of a TAGS file)
  (setq symbols-outline-fetch-fn #'symbols-outline-lsp-fetch)
  (symbols-outline-follow-mode)
  )


;;;;;;;;;;;;;;;;;;;;
;; Enable Elpy mode for more a advanced Python experience
;; https://realpython.com/emacs-the-best-python-editor/ and
;; https://elpy.readthedocs.io/en/latest/index.html
;; For optimal functionality, install the flake8 binary, as well as
;; the python-virtualenv package.
;; Note, this package also installs the Company completion framework.
(use-package elpy
  :ensure t
  :init
  (elpy-enable)
  :custom
  ;; Use Ipython as interpreter
  (python-shell-interpreter "ipython3")
  (python-shell-interpreter-args "-i --simple-prompt")
  :config
  ;; Enable code folding
  (add-to-list 'elpy-modules 'elpy-module-folding)
  )


;; Use company mode for (auto-)completion
;; http://company-mode.github.io/
(use-package company
  :ensure t
  :diminish
  :hook
  ;; Enable completion in all modes that inherit from prog-mode. Note
  ;; that for R (ESS) a connection to an R session is required,
  ;; because ESS uses R's built-in completion mechanism.
  ;; NOTE: no need to explicitly load Company for Python as Elpy
  ;; already does that.
  (prog-mode . company-mode)
  )

;;;;;;;;;;;;;;;;;;;;
(use-package yaml-mode
  :ensure t
  :mode
  (("\\.yml\\'" . yaml-mode)
   ("\\.yaml\\'" . yaml-mode))
  :hook
  ;; YAML mode doesn't derive from prog-mode, so we add it here.
  (yaml-mode . (lambda () (run-hooks 'prog-mode-hook)))
  )


;;;;;;;;;;;;;;;;;;;;
;; The Nexftlow language (www.nexflow.io) is derived from Groovy, so
;; we use groovy-mode for .nf files:
(use-package groovy-mode
  :ensure t
  :mode
  (("\\.nf\\'" . groovy-mode))
  )


;;;;;;;;;;;;;;;;;;;;
;;; Turn on line numbers on the left side of the screen for some
;;; modes.
(use-package display-line-numbers
  :ensure t
  :defer t
  :custom
  ;; Compute the width of the line numbers based on the actual nr. of
  ;; lines in the file. No idea yet if this will slow things down too
  ;; much for large files...
  (display-line-numbers-width-start t)
  :hook
  ;; Turn line numbers on in programming modes
  (prog-mode . display-line-numbers-mode)
  ;; Turn line numbers on for LaTeX documents/AucTeX
  (TeX-mode . display-line-numbers-mode)
  ;; Turn on line numbers for (R-)markdown files
  (markdown-mode . display-line-numbers-mode)
  ;; Turn on line numbers in config files
  (conf-mode . display-line-numbers-mode)
  )


;;;;;;;;;;;;;;;;;;;;
;; Load org-mode
;; https://org-mode.org
(use-package org
  :ensure t
  :bind (("C-c l" . org-store-link)
         ("C-c a" . org-agenda)
         )
  :hook
  ;; Let Org handle (visual) indentation (instead of manually
  ;; indenting). This also works better (visually) when using
  ;; mixed-pitch-mode.
  (org-mode . org-indent-mode)
  ;; Turn on auto fill
  (org-mode . auto-fill-mode)
  ;; Add spell checking
  (org-mode . flyspell-prog-mode)
  ;; In agenda view, highlight the line the cursor is currently on to
  ;; avoid mistakes
  (org-agenda-mode . hl-line-mode)
  :custom
  ;; Allow alphabetical lists (like a), b), etc.)
  (org-list-allow-alphabetical t)
  ;; C-a and C-e on headlines go to the start/end of the headline
  ;; first, instead of the start/end of the line. A second C-a/C-e
  ;; brings you to the start/end of the line.
  (org-special-ctrl-a/e t)
  ;; Highlight LaTeX stuff like $x^$ in Org buffers
  (org-highlight-latex-and-related '(native
                                     script
                                     entities))
  ;; Don't ask for confirmation when evaluating source blocks.
  ;; Take heed of the security implications!
  (org-confirm-babel-evaluate nil)
  ;; Tell Org to use Python 3 in Python code blocks, even if Python2
  ;; is present.
  (org-babel-python-command "python3")
  ;; Don't fontify source code blocks in org-mode. Enable this if
  ;; moving through an org file is slow.
  ;;(org-src-fontify-natively nil)
  :config
  ;; Setup org-babel for source code snippets
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((awk . t)
     (C . t)
     (ditaa . t)
     (dot . t)
     (gnuplot . t)
     (latex . t)
     (python . t)
     (R . t)
     (shell . t)
     (octave . t)
     ))
  )
;; end of use-package org-mode


;;;;;;;;;;;;;;;;;;;;
;; Enable Org-mode exports
(use-package ox
  :commands (org-export-dispatch)        ; (bound to C-c C-e)
  :config
  ;; Do export priority labels
  (setq org-export-with-priority t)

  ;; Don't export tags (in the section headers)
  (setq org-export-with-tags nil)

  ;;;;;;;;;;;;;;;;;;;;
  ;; Enable org export to LaTeX
  (use-package ox-latex
    :custom
    ;; Use the LaTeX listings package for code listings from Org mode
    (org-latex-src-block-backend 'listings)
    :config
    ;; Make XeLaTeX the default LaTeX compiler
    (setq org-latex-compiler "xelatex")
    ;; Set the default tex-to-pdf command. See
    ;; https://tecosaur.github.io/emacs-config/config.html#compiling
    ;; for details.
    (setq org-latex-pdf-process '("latexmk -f -%latex -shell-escape -interaction=nonstopmode -output-directory=%o %f"))


    ;; Redefine how numbers in scientific notation in tables are
    ;; exported:
    (setq org-latex-table-scientific-notation "$%s\\times 10^{%s}$")

    ;; Don't put table captions above the table when
    ;; exporting to LaTeX.
    (setq org-latex-caption-above nil)
    ;; Use booktabs for the horizontal rules in tables
    (setq org-latex-tables-booktabs t)

    ;; Set the document header (define what beamer and scrartcl mean)
    (unless (boundp 'org-latex-classes)
      (setq org-latex-classes nil))

    (add-to-list 'org-latex-classes
                 '("scrartcl"
                   "\\documentclass[11pt,a4paper]{scrartcl}"
                   ("\\section{%s}" . "\\section*{%s}")
                   ("\\subsection{%s}" . "\\subsection*{%s}")
                   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                   ("\\paragraph{%s}" . "\\paragraph*{%s}")
                   ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
                 )


    ;; The default org report export uses the first level
    ;; for Parts. I'm not interested in those when writing
    ;; reports. Use Chapters as the top level.
    (add-to-list 'org-latex-classes
                 '("scrreprt"
                   "\\documentclass[11pt,both,a4paper]{scrreprt}"
                   ("\\chapter{%s}" . "\\chapter*{%s}")
                   ("\\section{%s}" . "\\section*{%s}")
                   ("\\subsection{%s}" . "\\subsection*{%s}")
                   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                   ("\\paragraph{%s}" . "\\paragraph*{%s}")
                   ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
                 )


    ;; Set the default export class
    (setq org-latex-default-class "scrartcl")

    ;; Add some default packages to the LaTeX header for all classes:
    ;; Code listings
    (add-to-list 'org-latex-packages-alist '("" "listings"))
    ;; For coloured source code include the color package
    (add-to-list 'org-latex-packages-alist '("svgnames" "xcolor"))
    ;; Use the LaTeX listings package for code listings from Org mode
    ;(setq org-latex-listings t)
    ) ; End of use-package ox-latex

  ;;;;;;;;;;;;;;;;;;;;
  ;; Enable org export to org mode
  (use-package ox-org
    )

  ;;;;;;;;;;;;;;;;;;;;
  ;; Enable org export to odt
  (use-package ox-odt
    :config
    ;; In order to be able to convert LaTeX-style math to ODT, LaTeXML
    ;; is needed. See
    ;; https://orgmode.org/manual/LaTeX-math-snippets.html. On Ubuntu
    ;; install with sudo apt install latexml
    (setq org-latex-to-mathml-convert-command
      "latexmlmath %i --presentationmathml=%o")
    )

  ;;;;;;;;;;;;;;;;;;;;
  ;; Enable org export to MarkDown
  (use-package ox-md
    )

  ;;;;;;;;;;;;;;;;;;;;
  ;; Customisation of Org mode LaTeX export
  (use-package ox-beamer
    )
  ) ; End of use-package ox


;; Citations in Org mode
;; https://blog.tecosaur.com/tmio/2021-07-31-citations.html
(use-package oc
  :config
  (require 'oc-csl)
  (require 'oc-basic)
  ;; (require 'oc-bibtex)
  (require 'oc-biblatex)
  )
(use-package citeproc
  :ensure t
  )


;;;;;;;;;;;;;;;;;;;;
;; Emacs theme Modus Vivendi
;; See https://protesilaos.com/modus-themes/
(use-package modus-themes
  :ensure t
  :custom
  ;; Org source blocks get a light background, depending on their
  ;; language
  (modus-themes-org-blocks 'tinted-background)
  :config
  ;; modus-vivendi is a dark theme, change to modus-operandi for a
  ;; light theme.
  (load-theme 'modus-vivendi :no-confirm)
  )


;;;;;;;;;;;;;;;;;;;;
;; Modeline theme Moody
;; https://github.com/tarsius/moody
(use-package moody
  :ensure t
  :custom
  (x-underline-at-descent-line t)
  (moody-mode-line-height 20)
  :config
  (moody-replace-mode-line-buffer-identification)
  (moody-replace-vc-mode)
  (moody-replace-eldoc-minibuffer-message-function)
  )


;;;;;;;;;;;;;;;;;;;;
;; Setup AucTeX for LaTeX users
(use-package tex-site
  :ensure auctex
  :custom
  ;; Let AucTeX parse the document so it can recognise whether we use
  ;; BibTeX or Biblatex, etc. when loading
  (TeX-parse-self t)
  ;; Enable parsing on save
  (TeX-auto-save t)
  ;; Don't ask if the file must be saved when compiling a document
  ;; with changes
  (TeX-save-query nil)
  )


;; End of .emacs
